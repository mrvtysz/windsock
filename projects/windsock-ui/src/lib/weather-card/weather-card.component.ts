import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export interface WeatherCardModel {
  name: string;
  main: { humidity: number; pressure: number; temp: number };
  weather: { main: string }[];
  wind: {
    deg: number;
    speed: number;
  };
}

@Component({
  selector: 'ws-ui-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})
export class WeatherCardComponent implements OnInit {
  @Input() cityWeather: WeatherCardModel;
  @Input() units: string;
  @Output() cardSelected: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
