import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ws-ui-units-toggle',
  templateUrl: './units-toggle.component.html',
  styleUrls: ['./units-toggle.component.scss']
})
export class UnitsToggleComponent implements OnInit {

  @Input() units: string;
  @Output() buttonClicked: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
