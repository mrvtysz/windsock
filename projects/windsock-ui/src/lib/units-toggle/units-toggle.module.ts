import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnitsToggleComponent } from './units-toggle.component';



@NgModule({
  declarations: [UnitsToggleComponent],
  imports: [
    CommonModule
  ],
  exports: [UnitsToggleComponent]
})
export class UnitsToggleModule { }
