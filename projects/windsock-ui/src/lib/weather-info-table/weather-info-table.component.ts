import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ws-ui-weather-info-table',
  templateUrl: './weather-info-table.component.html',
  styleUrls: ['./weather-info-table.component.scss']
})
export class WeatherInfoTableComponent implements OnInit {

  @Input() weatherList;
  @Input() units: string;

  constructor() { }

  ngOnInit() {
  }

}
