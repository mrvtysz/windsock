import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherInfoTableComponent } from './weather-info-table.component';



@NgModule({
  declarations: [WeatherInfoTableComponent],
  imports: [
    CommonModule
  ],
  exports: [WeatherInfoTableComponent]
})
export class WeatherInfoTableModule { }
