import {
  NgModule,
  Optional,
  SkipSelf,
  ModuleWithProviders
} from '@angular/core';
import { WeatherService } from './services/weather.service';
import { ForecastService } from './services/forecast.service';
import { UnitsService } from './services/units.service';

@NgModule({
  declarations: [],
  imports: [],
  exports: []
})
export class WindsockDataModule {
  constructor(@Optional() @SkipSelf() parentModule: WindsockDataModule) {
    if (parentModule) {
      throw new Error(
        'NxDataProfileModule is already loaded. Import it in the AppModule only'
      );
    }
  }

  // INFO: This will provide environment file to the services to get the base url and the endpoints -- MT
  static forRoot(environment: any): ModuleWithProviders {
    return {
      ngModule: WindsockDataModule,
      providers: [
        WeatherService,
        ForecastService,
        UnitsService,
        { provide: 'environment', useValue: environment }
      ]
    };
  }
}
