import { CityWeather } from "./city-weather.interface";
import { City } from "./city.interface";

export interface WeatherResponse {
    cnt: number;
    city?: City;
    list: CityWeather[];
}