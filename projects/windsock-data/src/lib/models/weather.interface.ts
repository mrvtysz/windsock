export interface Weather {
    description: string;
    icon: string;
    id: number;
    in: string;
}