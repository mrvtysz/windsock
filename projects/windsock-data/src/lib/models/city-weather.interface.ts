import { Main } from './main.interface';
import { Weather } from './weather.interface';
import { Wind } from './wind.interface';

export interface CityWeather {
    id?: number;
    name?: string;
    main: Main;
    weather: Weather;
    wind: Wind;
    dt_txt?: string;
}