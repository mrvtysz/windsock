import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError} from 'rxjs';

import { retry, catchError } from 'rxjs/operators';

export abstract class BaseService {
  constructor(protected http: HttpClient) {}

  protected get<T>(endpoint: string, queryParams?: HttpParams): Observable<T> {
    return this.http.get<T>(endpoint, { params: queryParams }).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  protected post<T>(endpoint: string, body: any): Observable<T> {
    return this.http.post<T>(endpoint, body).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  protected put<T>(endpoint: string, body: any): Observable<T> {
    return this.http.put<T>(endpoint, body).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  protected delete<T>(endpoint: string): Observable<T> {
    return this.http.delete<T>(endpoint).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else if (error.code && error.description) {
      errorMessage = `${error.code}: ${error.description}`;
    } else {
      errorMessage = `${error.status}: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
