import { BaseService } from "./base.service";
import { HttpClient, HttpParams } from "@angular/common/http";
import { BehaviorSubject, ReplaySubject } from "rxjs";

export class ResourceService<T> extends BaseService {
  url: string;

  private data$: BehaviorSubject<T[]> = new BehaviorSubject([]);
  data = this.data$.asObservable();

  private queryParams$: BehaviorSubject<any> = new BehaviorSubject({
    units: "metric",
    id: ""
  });
  queryParams = this.queryParams$.asObservable();

  private selected$: ReplaySubject<T> = new ReplaySubject();
  selected = this.selected$.asObservable();

  constructor(protected http: HttpClient, private endpoint: string) {
    super(http);

    this.url = "http://api.openweathermap.org/data/2.5/group";
  }

  list(): void {
    this.data$.next([]);
    let queryParams = new HttpParams();
    queryParams = queryParams.set(
      "id",
      this.queryParams$.value["id"].toString()
    );
    queryParams = queryParams.set(
      "units",
      this.queryParams$.value["units"].toString()
    );

    this.get<any>(this.url, queryParams).subscribe(response => {
      //   this.queryParams$.next({
      //     ...this.queryParams$.value,
      //     ...{
      //       units: response["pageable"]["pageNumber"],

      //     }
      //   });
      this.data$.next(response["list"]);
    });
  }

  read(id: string): void {
    this.selected$.next(null);
    this.get<T>(`${this.url}/${id}`).subscribe(response => {
      this.selected$.next(response);
    });
  }

  setUnits(units: string) {
    this.queryParams$.next({
      ...this.queryParams$.value,
      ...{
        units: units
      }
    });
  }
}
