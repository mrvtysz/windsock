import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UnitsService {

  private units$: BehaviorSubject<any> = new BehaviorSubject('metric');
  units = this.units$.asObservable();

  constructor() { }

  setUnits(units) {
    this.units$.next(units);

  }
}
