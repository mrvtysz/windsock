import { Injectable, Inject } from '@angular/core';
import { BaseService } from './base.service';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { WeatherResponse } from '../models/weather-response.interface';
import { UnitsService } from './units.service';

@Injectable({
  providedIn: 'root'
})
export class ForecastService extends BaseService {
  url: string;
  appid: string;

  private data$: BehaviorSubject<WeatherResponse> = new BehaviorSubject(null);
  data = this.data$.asObservable();

  private queryParams$: BehaviorSubject<any> = new BehaviorSubject({
    units: 'metric',
    cnt: 10
  });
  queryParams = this.queryParams$.asObservable();

  constructor(
    protected http: HttpClient,
    private unitsService: UnitsService,
    @Inject('environment') private environment: any
  ) {
    super(http);
    const apiConfig = environment.api.forecast;
    this.url = `${apiConfig.host}/${apiConfig.endpoint}`;
    this.appid = apiConfig.appid;
    this.unitsService.units.subscribe(response => this.setUnits(response));
  }

  getById(id: number) {
    let queryParams = new HttpParams();
    queryParams = queryParams.set(
      'id',
      id.toString()
    );
    queryParams = queryParams.set(
      'units',
      this.queryParams$.value.units.toString()
    );
    queryParams = queryParams.set(
      'cnt',
      this.queryParams$.value.cnt.toString()
    );
    queryParams = queryParams.set('appid', this.appid);

    this.get<WeatherResponse>(this.url, queryParams).subscribe(
      response => this.data$.next(response)
    );
  }

  setUnits(units: string) {
    this.queryParams$.next({
      ...this.queryParams$.value,
      ...{
        units
      }
    });
  }

  toggleUnits() {
    if (this.queryParams$.value.units === 'imperial') {
      this.setUnits('metric');
    } else {
      this.setUnits('imperial');
    }
  }
}
