import { Injectable, Inject } from '@angular/core';
import { BaseService } from './base.service';
import { BehaviorSubject } from 'rxjs';
import { HttpParams, HttpClient } from '@angular/common/http';

import { WeatherResponse } from '../models/weather-response.interface';
import { CityWeather } from '../models/city-weather.interface';
import { UnitsService } from './units.service';

@Injectable({
  providedIn: 'root'
})
export class WeatherService extends BaseService {
  url: string;
  appid: string;

  private data$: BehaviorSubject<CityWeather[]> = new BehaviorSubject([]);
  data = this.data$.asObservable();

  private queryParams$: BehaviorSubject<any> = new BehaviorSubject({
    units: 'metric',
    ids: []
  });
  queryParams = this.queryParams$.asObservable();

  constructor(
    protected http: HttpClient,
    private unitsService: UnitsService,
    @Inject('environment') private environment: any
  ) {
    super(http);
    const apiConfig = environment.api['weather'];
    const defaultCities = environment.app.defaultCities;
    this.setIds(defaultCities.map(c => c.id));
    this.url = `${apiConfig.host}/${apiConfig.endpoint}`;
    this.appid = apiConfig.appid;
    this.unitsService.units.subscribe(response => this.setUnits(response));
  }

  listById(): void {
    let queryParams = new HttpParams();
    queryParams = queryParams.set(
      'id',
      this.queryParams$.value['ids'].toString()
    );
    queryParams = queryParams.set(
      'units',
      this.queryParams$.value['units'].toString()
    );
    queryParams = queryParams.set('appid', this.appid);

    this.get<WeatherResponse>(this.url, queryParams).subscribe(
      response => this.data$.next(response.list)
    );

  }



  setUnits(units: string) {
    this.queryParams$.next({
      ...this.queryParams$.value,
      ...{
        units
      }
    });
  }

  setIds(ids: number[]) {
    this.queryParams$.next({
      ...this.queryParams$.value,
      ...{
        ids: ids.join(',')
      }
    });
  }

  toggleUnits() {
    if (this.queryParams$.value['units'] === 'imperial') {
      this.setUnits('metric');
    } else {
      this.setUnits('imperial');
    }
  }
}
