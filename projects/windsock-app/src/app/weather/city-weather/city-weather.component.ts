import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ForecastService } from 'projects/windsock-data/src/lib/services/forecast.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.scss']
})
export class CityWeatherComponent implements OnInit {
  cityId: number;
  data$: any;
  queryParams$: any;
  cityName: string;

  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Temperature' },
    { data: [], label: 'Wind' }
  ];
  lineChartLabels: Label[] = [];

  lineChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left'
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)'
          },
          ticks: {
            fontColor: 'red'
          }
        }
      ]
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private forecastService: ForecastService,
    private datePipe: DatePipe
  ) {
    this.data$ = this.forecastService.data;
    this.queryParams$ = this.forecastService.queryParams;
    this.data$.subscribe(response => {
      if (response && response.list) {
        this.lineChartData = [
          { data: response.list.map(l => l.main.temp), label: 'Temperature' },
          { data: response.list.map(l => l.wind.speed), label: 'Wind', yAxisID: 'y-axis-1' }
        ];
        this.lineChartLabels = response.list.map(l =>
          this.datePipe.transform(l.dt_txt, 'EEEEEE, HH:mm')
        );
        this.cityName = `${response.city.name},${response.city.country}`;
      }
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(response => {
      this.cityId = +response['id'];
      this.forecastService.getById(this.cityId);
    });
  }

  toggleUnits(e) {
    this.forecastService.toggleUnits();
    this.forecastService.getById(this.cityId);
  }
}
