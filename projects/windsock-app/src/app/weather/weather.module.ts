import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CityWeatherListComponent } from './city-weather-list/city-weather-list.component';
import { WeatherCardModule } from 'projects/windsock-ui/src/lib/weather-card/weather-card.module';
import { RouterModule } from '@angular/router';
import { CityWeatherComponent } from './city-weather/city-weather.component';
import { WeatherInfoTableModule } from 'projects/windsock-ui/src/lib/weather-info-table/weather-info-table.module';
import { ChartsModule } from 'ng2-charts';
import { UnitsToggleModule } from 'projects/windsock-ui/src/lib/units-toggle/units-toggle.module';



@NgModule({
  declarations: [CityWeatherListComponent, CityWeatherComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(  [{ path: '', component: CityWeatherListComponent },{ path: ':id', component: CityWeatherComponent }]),
    WeatherCardModule,
    WeatherInfoTableModule,
    ChartsModule,
    UnitsToggleModule
  ],
  exports: [CityWeatherListComponent],
  providers: [DatePipe]
})
export class WeatherModule { }
