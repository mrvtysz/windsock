import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'projects/windsock-data/src/public-api';
import { Observable } from 'rxjs';
import { CityWeather } from 'projects/windsock-data/src/lib/models/city-weather.interface';
import { Router } from '@angular/router';


@Component({
  selector: 'app-city-weather-list',
  templateUrl: './city-weather-list.component.html',
  styleUrls: ['./city-weather-list.component.scss']
})
export class CityWeatherListComponent implements OnInit {

  cityWeatherData$: Observable<CityWeather[]> ;
  queryParams$: Observable<any>;

  constructor(private weatherService: WeatherService, private router: Router) {
    this.cityWeatherData$ = this.weatherService.data;
    this.queryParams$ = this.weatherService.queryParams;
  }

  ngOnInit() {
    this.weatherService.listById();
  }

  goToCity(e) {
    this.router.navigate(['e']);
  }

  toggleUnits(e) {
    this.weatherService.toggleUnits();
    this.weatherService.listById();
  }

}
