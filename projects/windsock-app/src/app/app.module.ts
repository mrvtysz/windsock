import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { WindsockDataModule } from 'projects/windsock-data/src/public-api';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HeaderModule } from 'projects/windsock-ui/src/lib/header/header.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: "weather",
        loadChildren: () =>
          import("./weather/weather.module").then(mod => mod.WeatherModule)
      },
      {
        path: "",
        redirectTo: "/weather",
        pathMatch: "full"
      }
    ]),
    HttpClientModule,
    HeaderModule,
    WindsockDataModule.forRoot(environment)
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
