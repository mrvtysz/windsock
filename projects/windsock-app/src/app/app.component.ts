import { Component } from "@angular/core";
import { UnitsService } from "projects/windsock-data/src/public-api";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "windsock-app";
}
