export const environment = {
  production: false,
  logging: {
    console: true
  },
  api: {
    weather: {
      host: 'https://api.openweathermap.org/data/2.5',
      endpoint: 'group',
      appid: '9ad5e8a9dd98c23f8a655c2534cd8f24'
    },
    forecast: {
      host: 'https://api.openweathermap.org/data/2.5',
      endpoint: 'forecast',
      appid: '9ad5e8a9dd98c23f8a655c2534cd8f24'
    }
  },
  app: {
    defaultCities: [
      { name: 'Istanbul', id: 745044 },
      { name: 'Amsterdam', id: 2759794 },
      { name: 'Lisbon', id: 2267057 },
      { name: 'Berlin', id: 2950159 },
      { name: 'Turku', id: 633679 }
    ]
  }
};
