# Windsock

Demo: https://windsock.herokuapp.com/

Font: Roboto as stated in Backbase Style Guide


This project consists of one application and two libraries.

## Windsock App

An Angular application which has a Weather module which has two routes, /weather and /weather/:id. Weather shows the current weather situation of 5 cities: Istanbul, Amsterdam, Lisbon, Berlin and Turku.

This application may scale so this weather module is seperate.

## Windsock Ui

Windsock Ui has presentational component modules.

## Windsock Data

Windsock Data has data access and models.

## TO DO

Write tests
